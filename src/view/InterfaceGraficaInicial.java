package view;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import controllers.AcoesInterfaceInicial;





public class InterfaceGraficaInicial  {
	
	private JPanel painelInicial;
	private JLabel painelImagem;
	
	public InterfaceGraficaInicial(JFrame telaInicial) throws IOException{
		
		criaPainelInicial(telaInicial);
		
		
	}
	
	public DefaultListModel<String> lerDadosFilmes(DefaultListModel<String> listaFilmes) throws IOException {
		
		
		@SuppressWarnings("resource")
		BufferedReader dados = new BufferedReader(new FileReader("Arquivos/filmes.txt"));
		String dadoCadaFilme;
		String filme="";
		while(dados.ready()){
			filme=dados.readLine();
			String ano=dados.readLine();
			String genero=dados.readLine();
			dadoCadaFilme = "Filme: " + filme +" ---- "+  "Ano de lançamento: " +ano+" ---- "+
					"Genero: " + genero;
			listaFilmes.addElement(dadoCadaFilme);
		}	
		
		return listaFilmes;
		
		
	}
	
	@SuppressWarnings("rawtypes")
	public void criaPainelInicial(JFrame telaInicial) throws IOException{
		
		
		
		painelInicial = new JPanel();
		painelInicial.setLayout(null);
		painelInicial.setLocation((telaInicial.getWidth()-painelInicial.getWidth())/2,(telaInicial.getHeight()-painelInicial.getHeight())/2);
		
		
		JButton cadastrar = new JButton("Cadastrar novo filme");
		cadastrar.setBounds(345,400,310,30);
		

		System.getProperty("line.separator");
		DefaultListModel<String> modelLista = new DefaultListModel<String>();
		modelLista.setSize(30);
	    
		@SuppressWarnings("unchecked")
		JList lista = new JList(lerDadosFilmes(modelLista));
		lista.setBounds(500, 200,400, 150);
		JScrollPane listScroller = new JScrollPane(lista);
		listScroller.setPreferredSize(new Dimension(20,20));
		listScroller.setBounds(150, 200,700, 150);
		
		painelImagem = new JLabel();
		painelImagem.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		painelImagem.setBounds(0, 0, 1280,145	);
		ImageIcon imagemInicial = new ImageIcon("Imagens/rsz_interstellar.png");
		Image image = imagemInicial.getImage();
		Image newimg = image.getScaledInstance(1280, 145,  java.awt.Image.SCALE_SMOOTH);
		imagemInicial = new ImageIcon(newimg);
		painelImagem.setIcon(imagemInicial);
		telaInicial.add(painelImagem);
		telaInicial.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		
		
		
	
		cadastrar.setActionCommand("Cadastrar novo filme");
		cadastrar.addActionListener(new AcoesInterfaceInicial(painelInicial,telaInicial));
		telaInicial.add(painelInicial);
		painelInicial.add(cadastrar);
	
		painelInicial.add(listScroller);

		telaInicial.setVisible(true);
	
		}
		
		
	}
	


